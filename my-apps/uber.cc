#include "../include/led-matrix.h"
#include "../include/graphics.h"

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <cstdlib>

using rgb_matrix::GPIO;
using rgb_matrix::RGBMatrix;
using rgb_matrix::Canvas;
using namespace std;
//using namespace rgb_matrix;



int main(int argc, char *argv[]) {
  rgb_matrix::RuntimeOptions runtime_opt;
  runtime_opt.gpio_slowdown = 2;

  RGBMatrix::Options defaults;
  defaults.hardware_mapping = "regular";  // or e.g. "adafruit-hat"
  defaults.rows = 32;
  defaults.cols = 64;
  defaults.chain_length = 2;
  //defaults.parallel = 1;
  defaults.show_refresh_rate = false;
//  Canvas *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults);
  RGBMatrix *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults, &runtime_opt);
  if (canvas == NULL)
    return 1;

  //canvas->SetBrightness(50);
	rgb_matrix::Font uberFont;
  uberFont.LoadFont("../fonts/6x13B.bdf");
	
  rgb_matrix::Font font;
  font.LoadFont("../fonts/5x8.bdf");
	rgb_matrix::Font bottomFont;
  bottomFont.LoadFont("../fonts/6x13.bdf");
  //Color color(255, 255, 0);
  rgb_matrix::Color color(88, 140, 196);
  rgb_matrix::Color blue_color(0, 0, 255);
  rgb_matrix::Color green(116, 206, 185);
  rgb_matrix::Color red(255, 0, 0);
  rgb_matrix::Color yel(187, 200, 191);
  //int letter_spacing = 2;
	
	//std::string time;
	string strFileContent;
	fstream inoutfile;
	while(true){
		
		inoutfile.open("test.txt", ios_base::in);
		cout << inoutfile.fail();
		cout << inoutfile.bad();
		cout << inoutfile.eof();
		if(inoutfile.good()){
			inoutfile >> strFileContent; // only one I/O
		}
//printf(strFileContent);
//sleep(100);
		//std::cout << strFileContent; // this is also one I/O
		//std::cout << "test";
		//sleep(100);
		//row 1
		rgb_matrix::DrawText(canvas, uberFont, 0,10, color, NULL, "Uber", 1);
		rgb_matrix::DrawText(canvas, font, 30,10, red, NULL, "Toyota Prius", 0);
		rgb_matrix::DrawText(canvas, font, 93,10, yel, NULL, "7UI3JHC", 0);

		//row 2
		rgb_matrix::DrawText(canvas, bottomFont, 0,30, blue_color, NULL, "John 4.1 stars", 0);
		rgb_matrix::DrawText(canvas, bottomFont, 93,30, green, NULL, strFileContent.c_str(), 0);
		//printf(time);
		sleep(300);
	}


  //canvas->SetPixel(1, 1, 255, 0, 0);
  // usleep(3 * 100000);  // wait a little to slow down things.
  sleep (100);
}
