#include <stdio.h>
#include <iostream>
#include <string>
#include <curl/curl.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

int main(){
  CURL *curl;
  CURLcode res;
  std::string readBuffer;
  struct curl_slist *headerList = NULL;
  headerList = curl_slist_append(headerList, "Content-Type: application/json");
  headerList = curl_slist_append(headerList, "Authorization: Bearer KA.eyJ2ZXJzaW9uIjoyLCJpZCI6Inptc1p4SDRpUzAybWJOcG4yMmg0Mmc9PSIsImV4cGlyZXNfYXQiOjE1MjY3NDE3ODYsInBpcGVsaW5lX2tleV9pZCI6Ik1RPT0iLCJwaXBlbGluZV9pZCI6MX0.-WKRcMT6BcpYG6l1L6l84bWyPJb3Oa_lE9A2PmldctI");
  headerList = curl_slist_append(headerList, "Accept-Language: en_US");

  curl = curl_easy_init();

  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://sandbox-api.uber.com/v1.2/requests/current");
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);

    std::cout << readBuffer << std::endl;
    auto response = json::parse(readBuffer);
    std::cout << response["status"];
  }

  return 0;
}
