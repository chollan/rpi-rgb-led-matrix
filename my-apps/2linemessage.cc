#include "../include/led-matrix.h"
#include "../include/graphics.h"

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

using rgb_matrix::GPIO;
using rgb_matrix::RGBMatrix;
using rgb_matrix::Canvas;
using namespace std;
//using namespace rgb_matrix;

int main(int argc, char *argv[]) {
  rgb_matrix::RuntimeOptions runtime_opt;
  runtime_opt.gpio_slowdown = 2;
  runtime_opt.daemon = true;


  RGBMatrix::Options defaults;
  defaults.hardware_mapping = "regular";  // or e.g. "adafruit-hat"
  defaults.rows = 32;
  defaults.cols = 64;
  defaults.chain_length = 2;
  defaults.parallel = 1;
  //defaults.show_refresh_rate = true;
//  Canvas *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults);
  RGBMatrix *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults, &runtime_opt);
  if (canvas == NULL)
    return 1;
		
	int opt;
  std::string line1;
  std::string line2;
  double time = 120.0;
  int brightness = 100;
  //rgb_matrix::Color color(0,0,0);
  while ((opt = getopt(argc, argv, "1:2:b:s:")) != -1) {
    switch (opt) {
			case '1': line1 = strdup(optarg); break;
			case '2': line2 = strdup(optarg); break;
			case 'b': brightness = atoi(optarg); break;
			case 's': time = atof(optarg); break;
			//case 'b': brightness = atoi(optarg); break;
			//default: return usage(argv[0]);
    }
  }
	
  canvas->SetBrightness(brightness);
  rgb_matrix::Font font;
  font.LoadFont("../fonts/6x13.bdf");
  //font.LoadFont("../fonts/4x6.bdf");
  //Color color(255, 255, 0);
  rgb_matrix::Color color(255, 255, 255);
  
  //row 1
  rgb_matrix::DrawText(canvas, font, 0,13, color, NULL, line1.c_str(), 0);
  rgb_matrix::DrawText(canvas, font, 0,26, color, NULL, line2.c_str(), 0);
  //rgb_matrix::DrawText(canvas, font, 0,26, red, NULL, "Jaime :) :*", 0);

  //row 2
  //rgb_matrix::DrawText(canvas, font, 0,15, blue_color, NULL, "John 4.1 stars", 0);
  //rgb_matrix::DrawText(canvas, font, 70,15, green, NULL, "6 min", 0);

  //canvas->SetPixel(1, 1, 255, 0, 0);
  // usleep(3 * 100000);  // wait a little to slow down things.
  sleep (time);

  canvas->Clear();
  return 0;
}
