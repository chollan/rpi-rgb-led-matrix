#include "../include/led-matrix.h"
#include "../include/graphics.h"

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <curl/curl.h>
#include <nlohmann/json.hpp>

using rgb_matrix::GPIO;
using rgb_matrix::RGBMatrix;
using rgb_matrix::Canvas;
using namespace std;
using json = nlohmann::json;
//using namespace rgb_matrix;

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp){
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

static int usage(const char *progname) {
  fprintf(stderr, "usage: %s [options]\n", progname);
  fprintf(stderr, "Fetches uber updates using the API key specified and displays it\n");
  fprintf(stderr, "Options:\n");
  //rgb_matrix::PrintMatrixFlags(stderr);
  fprintf(stderr,
          "\t-k <apiKey>       : API Key to call Uber\n"
          "\t-b <brightness>   : Brightness of the screen. (default:100)\n"
          );
  return 1;
}
//KA.eyJ2ZXJzaW9uIjoyLCJpZCI6Inptc1p4SDRpUzAybWJOcG4yMmg0Mmc9PSIsImV4cGlyZXNfYXQiOjE1MjY3NDE3ODYsInBpcGVsaW5lX2tleV9pZCI6Ik1RPT0iLCJwaXBlbGluZV9pZCI6MX0.-WKRcMT6BcpYG6l1L6l84bWyPJb3Oa_lE9A2PmldctI
static json getUpdatedStatus(string key){
  CURL *curl;
  CURLcode res;
  std::string readBuffer;
  struct curl_slist *headerList = NULL;
	string authPreString = "Authorization: Bearer " + key;
	char *authString = new char[authPreString.length()+1];
	strcpy(authString, authPreString.c_str());
  headerList = curl_slist_append(headerList, "Content-Type: application/json");
  //headerList = curl_slist_append(headerList, authString);
	//headerList = curl_slist_append(headerList, "Authorization: Bearer KA.eyJ2ZXJzaW9uIjoyLCJpZCI6Inptc1p4SDRpUzAybWJOcG4yMmg0Mmc9PSIsImV4cGlyZXNfYXQiOjE1MjY3NDE3ODYsInBpcGVsaW5lX2tleV9pZCI6Ik1RPT0iLCJwaXBlbGluZV9pZCI6MX0.-WKRcMT6BcpYG6l1L6l84bWyPJb3Oa_lE9A2PmldctI");
	headerList = curl_slist_append(headerList, "Authorization: Bearer KA.eyJ2ZXJzaW9uIjoyLCJpZCI6IjVBaHlzVjBkVC9hWXBONk1LTGVZNXc9PSIsImV4cGlyZXNfYXQiOjE1MjcwMzc3OTYsInBpcGVsaW5lX2tleV9pZCI6Ik1RPT0iLCJwaXBlbGluZV9pZCI6MX0.gvCiUT0qyqsQtahLrfa8UdMQRhgQWmRh5kqfbdW9b5Q");
  headerList = curl_slist_append(headerList, "Accept-Language: en_US");

  curl = curl_easy_init();
	auto response = json::parse("{}");
  if(curl) {
		// DEV
    //curl_easy_setopt(curl, CURLOPT_URL, "https://sandbox-api.uber.com/v1.2/requests/current");
		// PROD
		curl_easy_setopt(curl, CURLOPT_URL, "https://api.uber.com/v1.2/requests/current");
  	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);

    std::cout << readBuffer << std::endl;
    response = json::parse(readBuffer);
    //std::cout << response["status"];
  }else{
    std::cout << "no curl";
  }
  std::cout << response;
  return response;
}


int main(int argc, char *argv[]) {
  rgb_matrix::RuntimeOptions runtime_opt;
  runtime_opt.gpio_slowdown = 2;
  runtime_opt.daemon = true;

	int opt;
  string apiKey;
  int brightness = 100;
  while ((opt = getopt(argc, argv, "k:b:")) != -1) {
    switch (opt) {
			case 'k': apiKey = atof(optarg); break;
			case 'b': brightness = atoi(optarg); break;
			default:
				return usage(argv[0]);
    }
  }
	
	if(apiKey == ""){
		printf("Missing API Key\n");
		return 1;
	}
	
	json currentStatus = getUpdatedStatus(apiKey);
	if(currentStatus["status"] == NULL){
		// there is no ride currently
		return 0;
	}
	
  RGBMatrix::Options defaults;
  defaults.hardware_mapping = "regular";  // or e.g. "adafruit-hat"
  defaults.rows = 32;
  defaults.cols = 64;
  defaults.chain_length = 2;
  //defaults.parallel = 1;
  defaults.show_refresh_rate = false;
//  Canvas *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults);
  RGBMatrix *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults, &runtime_opt);
  if (canvas == NULL)
    return 1;
	if(currentStatus["status"] != "accepted" && currentStatus["status"] != "arriving")
		return 1;
	
  canvas->SetBrightness(brightness);
	rgb_matrix::Font uberFont;
  uberFont.LoadFont("../fonts/6x13B.bdf");
	
  rgb_matrix::Font font;
  font.LoadFont("../fonts/5x8.bdf");
	rgb_matrix::Font bottomFont;
  bottomFont.LoadFont("../fonts/6x13.bdf");
  //Color color(255, 255, 0);
  rgb_matrix::Color color(88, 140, 196);
  rgb_matrix::Color blue_color(0, 0, 255);
  rgb_matrix::Color green(116, 206, 185);
  rgb_matrix::Color red(255, 0, 0);
  rgb_matrix::Color yel(187, 200, 191);
  //int letter_spacing = 2;
	
	//std::string time;
	//string strFileContent;
	//fstream inoutfile;
	string minutesString;
	string vehicle = currentStatus["vehicle"]["make"].get<std::string>() + " " + currentStatus["vehicle"]["model"].get<std::string>();
	string plate = currentStatus["vehicle"]["license_plate"].get<std::string>();
	string name = currentStatus["driver"]["name"].get<std::string>() + " " + std::to_string(static_cast<long long>(currentStatus["driver"]["rating"])) + " stars";
	string name_without_start = currentStatus["driver"]["name"].get<std::string>() + " " + std::to_string(static_cast<long long>(currentStatus["driver"]["rating"]));
	bool terminateLoop;
	while(true){
		terminateLoop = true;
		canvas->Clear();
		if(currentStatus["status"] == "accepted" || currentStatus["status"] == "arriving"){
			// build a new minutes string
			minutesString = std::to_string(static_cast<long long>(currentStatus["pickup"]["eta"])) + " min";
			
			//row 1
			rgb_matrix::DrawText(canvas, uberFont, 0,10, color, NULL, "Uber", 1);
			rgb_matrix::DrawText(canvas, font, 0,19, red, NULL, vehicle.c_str(), 0);
			rgb_matrix::DrawText(canvas, font, 66,10, yel, NULL, plate.c_str(), 0);

			//row 2
			rgb_matrix::DrawText(canvas, bottomFont, 0,30, blue_color, NULL, name.c_str(), 0);
			if(currentStatus["status"] == "accepted"){
				rgb_matrix::DrawText(canvas, bottomFont, 93,30, green, NULL, minutesString.c_str(), 0);
			}else{
				rgb_matrix::DrawText(canvas, bottomFont, 85,30, green, NULL, "ARRIVE", 0);
			}	
			terminateLoop = false;
		}
		if(terminateLoop){
			break;
		}else{
			sleep(3);
			currentStatus = getUpdatedStatus(apiKey);
		}
	}
	canvas->Clear();
	return 0;

  //canvas->SetPixel(1, 1, 255, 0, 0);
  // usleep(3 * 100000);  // wait a little to slow down things.
  sleep (100);
}
