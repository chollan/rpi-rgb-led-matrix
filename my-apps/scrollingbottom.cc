#include "led-matrix.h"
#include "graphics.h"

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <ctime>
//#include <iostream>

using rgb_matrix::GPIO;
using rgb_matrix::RGBMatrix;
using rgb_matrix::Canvas;

volatile bool interrupt_received = false;
static void InterruptHandler(int signo) {
  interrupt_received = true;
}

static int usage(const char *progname) {
  fprintf(stderr, "usage: %s [options] <text>\n", progname);
  fprintf(stderr, "Takes warning text and displays it\n");
  fprintf(stderr, "Options:\n");
  //rgb_matrix::PrintMatrixFlags(stderr);
  fprintf(stderr,
          "\t-1 <line 1 text>   : Download alert type (sonarr, radarr)\n"
          "\t-2 <line 2 text>  : Download message.\n"
          "\t-t <runtime>      : Message runtime. (default: 120 sec)\n"
          "\t-b <brightness>   : Brightness of the screen. (default:100)\n"
          );
  return 1;
}

int main(int argc, char *argv[]) {
  signal(SIGTERM, InterruptHandler);
  signal(SIGINT, InterruptHandler);

  rgb_matrix::RuntimeOptions runtime_opt;
  runtime_opt.gpio_slowdown = 1;
  runtime_opt.daemon = true;

  RGBMatrix::Options defaults;
  defaults.hardware_mapping = "regular";  // or e.g. "adafruit-hat"
  defaults.rows = 32;
  defaults.cols = 64;
  defaults.chain_length = 2;
  defaults.parallel = 1;
  //defaults.show_refresh_rate = true;
//  Canvas *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults);
  RGBMatrix *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults, &runtime_opt);
  if (canvas == NULL)
    return 1;

  rgb_matrix::Font large_font;
  large_font.LoadFont("../fonts/6x13.bdf");

  //rgb_matrix::Font font;
  //font.LoadFont("../fonts/5x7.bdf");

  int opt;
  std::string line1;
  std::string line2;
  double time = 120.0;
  int brightness = 100;
  rgb_matrix::Color color(255,255,255);
  while ((opt = getopt(argc, argv, "1:2:t:b:")) != -1) {
    switch (opt) {
      case '1': line1 = strdup(optarg); break;
      case '2': line2 = strdup(optarg); break;
      case 'b': brightness = atof(optarg); break;
      case 't': time = atof(optarg); break;
      default: return usage(argv[0]);
    }
  }

  // set the brithtness
  if (brightness < 1 || brightness > 100) {
    fprintf(stderr, "Brightness is outside usable range.\n");
    return 1;
  }else{
    canvas->SetBrightness(brightness);
  }


  // show opening screen
  //rgb_matrix::DrawText(canvas, large_font, 15,12, color, NULL, application.c_str(), 0);

  // wait a while
  //sleep(5);

  // start a timer
  std::clock_t start;
  start = std::clock();
  double duration;

  // start the display
  rgb_matrix::FrameCanvas *offscreen_canvas = canvas->CreateFrameCanvas();
  int x_orig = (defaults.chain_length * defaults.rows) + 50;
  int x = x_orig;
  int y = 26;
  int length = 0;
  while (!interrupt_received){
    //rgb_matrix::DrawText(canvas, font, 0,7, color, NULL, eventName.c_str(), 0);
    rgb_matrix::DrawText(canvas, large_font, 0,13, color, NULL, line1.c_str(), 0);
    offscreen_canvas->Clear(); // clear canvas

    length = rgb_matrix::DrawText(canvas, large_font, x, y, color, NULL, line2.c_str(), 0);
    if (--x + length < 0)
      x = x_orig;
    //if(x == 0)
    //  sleep(2.5);


    usleep(25000);
    // Swap the offscreen_canvas with canvas on vsync, avoids flickering
    offscreen_canvas = canvas->SwapOnVSync(offscreen_canvas);

    // get the duration and exit
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    if(duration > time){
      interrupt_received = true;
    }
  }

  // Finished. Shut down the RGB matrix.
  canvas->Clear();
  delete canvas;

  write(STDOUT_FILENO, "\n", 1);  // Create a fresh new line after ^C on screen
  return 0;
}
