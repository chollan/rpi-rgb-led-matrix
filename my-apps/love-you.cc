#include "../include/led-matrix.h"
#include "../include/graphics.h"

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

using rgb_matrix::GPIO;
using rgb_matrix::RGBMatrix;
using rgb_matrix::Canvas;
//using namespace rgb_matrix;

int main(int argc, char *argv[]) {

  RGBMatrix::Options defaults;
  defaults.hardware_mapping = "regular";  // or e.g. "adafruit-hat"
  defaults.rows = 32;
  defaults.cols = 64;
  defaults.chain_length = 2;
  defaults.parallel = 1;
  defaults.show_refresh_rate = true;
//  Canvas *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults);
  RGBMatrix *canvas = rgb_matrix::CreateMatrixFromFlags(&argc, &argv, &defaults);
  if (canvas == NULL)
    return 1;

//  canvas->SetBrightness(50);
  rgb_matrix::Font font;
  //font.LoadFont("../fonts/6x13.bdf");
  font.LoadFont("../fonts/4x6.bdf");
  //Color color(255, 255, 0);
  rgb_matrix::Color color(88, 140, 196);
  rgb_matrix::Color blue_color(0, 0, 255);
  rgb_matrix::Color green(116, 206, 185);
  rgb_matrix::Color red(255, 0, 0);
  rgb_matrix::Color yel(187, 200, 191);
  int letter_spacing = 2;

  //row 1
  rgb_matrix::DrawText(canvas, font, 0,5, red, NULL, "I LOVE YOU, JAIME :) :*", 0);
  rgb_matrix::DrawText(canvas, font, 0,10, red, NULL, "I love you, Jaime :) :*", 0);
  rgb_matrix::DrawText(canvas, font, 0,15, red, NULL, "I love you, Jaime :) :*", 0);
  rgb_matrix::DrawText(canvas, font, 0,20, red, NULL, "I love you, Jaime :) :*", 0);
  rgb_matrix::DrawText(canvas, font, 0,25, red, NULL, "I love you, Jaime :) :*", 0);
  //rgb_matrix::DrawText(canvas, font, 0,26, red, NULL, "Jaime :) :*", 0);

  //row 2
  //rgb_matrix::DrawText(canvas, font, 0,15, blue_color, NULL, "John 4.1 stars", 0);
  //rgb_matrix::DrawText(canvas, font, 70,15, green, NULL, "6 min", 0);

  //canvas->SetPixel(1, 1, 255, 0, 0);
  // usleep(3 * 100000);  // wait a little to slow down things.
  sleep (500);
}
